pipeline {
  agent any;

  options {
    buildDiscarder(logRotator(numToKeepStr: '6'))
  }
  
  environment {
    UPSTREAM_NAME = 'docker-sonarr'
    CONTAINER_NAME = 'sonarr-tv'
    scannerHome = tool 'TkfSonarqube'

    SONARR_BRANCH = "main"
  }

  stages {
    stage('Setup Environment') {
      steps {
        script {
          env.EXIT_STATUS = ''
          env.CURR_DATE = sh(
            script: '''date '+%Y-%m-%d_%H:%M:%S%:z' ''',
            returnStdout: true).trim()
          env.GITHASH_SHORT = sh(
            script: '''git log -1 --format=%h''',
            returnStdout: true).trim()
          env.GITHASH_LONG = sh(
            script: '''git log -1 --format=%H''',
            returnStdout: true).trim()
          env.GIT_TAG_NAME = sh(
            script: '''git describe --tags ${commit}''',
            returnStdout: true).trim()
          currentBuild.displayName = "${GIT_TAG_NAME}"

          env.SONARR_VERSION = sh(
            script: ''' curl -sX GET http://services.sonarr.tv/v1/releases | jq -r '.[] | select(.branch=="main") | .version' ''',            returnStdout: true).trim()

          sh "echo ******* SONARR_VERSION: ${SONARR_VERSION}"
        }
      }
    }
    stage('SonarQube analysis') {
      steps {
        withSonarQubeEnv('TkfSonarqube') {
          sh "${scannerHome}/bin/sonar-scanner"
        }
      }
    }
    stage('Buildx') {
      agent {
        label 'x86_64'
      }
      steps {
        echo "Running on node: ${NODE_NAME}"
        withDockerRegistry(credentialsId: 'teknofile-dockerhub', url: "https://index.docker.io/v1/") {
          deleteDir() /* clean up our workspace */
          sh '''
            git clone https://github.com/linuxserver/${UPSTREAM_NAME}.git
            cd ${UPSTREAM_NAME}

            docker buildx create --name tkf-builder --bootstrap --use
            docker buildx build \
            --platform linux/arm64,linux/amd64 \
            --build-arg SONARR_VERSION=${SONARR_VERSION} \
            -t teknofile/${CONTAINER_NAME} \
            -t teknofile/${CONTAINER_NAME}:latest \
            -t teknofile/${CONTAINER_NAME}:${GITHASH_LONG} \
            -t teknofile/${CONTAINER_NAME}:${GITHASH_SHORT} \
            -t teknofile/${CONTAINER_NAME}:${SONARR_VERSION} \
            . \
            --push
          '''
        }
      }
    }
    stage('Update Helm Chart') {
      steps {
        script {
          sh '''
            # Update the Chart Version with the tag from git
            sed -i 's/version:.*/version: '"$GIT_TAG_NAME"'/g' helm/Chart.yaml

            # Update Chart App Version with the version of Unifi
            sed -i 's/appVersion:.*/appVersion: '"${SONARR_VERSION}"'/g' helm/Chart.yaml

            helm lint helm            

            helm package helm
            helm push-artifactory *.tgz tkf-charts
          '''
        }
      }
    }
  }
  post {
    always {
      echo 'Cleaning up.'
      deleteDir() /* clean up our workspace */
    }
  }
}
